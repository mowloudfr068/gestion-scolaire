<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        
        $this->call([
            TypeBloodSeeder::class,
            NationalitieSeeder::class,
            ReligionSeeder::class,
            SpecializationsSeeder::class,
            GenderSeeder::class,
            GradeSeeder::class,
            ClassroomSeeder::class,
            TeacherSeeder::class,
            SectionSeeder::class,
            ParentSeeder::class,
            StudentSeeder::class,
            UserSeeder::class,



        ]);
    }
}
