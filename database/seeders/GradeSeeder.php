<?php

namespace Database\Seeders;

use App\Models\Grade;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grades')->delete();
        $gradesTable  = [
            [
                'Name' => [
                'fr' => 'Fondamentale',
                'ar' => 'الإبتدائية'
                ],
                'Notes' => 'bien',
            ],
            [
                'Name' => [
                    'fr' => 'Collège',
                    'ar' => 'الإعدادية',
                    ],
                
                'Notes' => 'bien',
            ],
            [
                'Name' => [
                    'fr' => 'Lycée',
                    'ar' => 'الثانوية',
                    ],
                'Notes' => 'bien',
            ],
        ];

        foreach($gradesTable as $grade){
            Grade::create($grade);
        }
    }
}
