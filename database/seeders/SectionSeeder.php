<?php

namespace Database\Seeders;

use App\Models\Section;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Nette\Utils\Random;

class SectionSeeder extends Seeder
{
   
    public function run()
    {
        DB::table('sections')->delete();
        $sectionsTable  = [
            [
                'Name' => [
                'fr' => '1',
                'ar' => '1'
                ],
                'status' =>1,
                'grade_id' =>1,
                'classroom_id' =>1,
            ],
            [
                'Name' => [
                    'fr' => '1',
                    'ar' => '1',
                    ],
                    'status' =>1,
                    'grade_id' =>1,
                    'classroom_id' =>2,
                
            ],
            [
                'Name' => [
                    'fr' => '1',
                    'ar' => '1',
                    ],
                    'status' =>1,
                    'grade_id' =>1,
                    'classroom_id' =>3,
            ],
        ];

        foreach($sectionsTable as $section){
           $s= Section::create($section);
            $s->teachers()->attach(random_int(1,3));
        }
    }
}
