<?php

namespace Database\Seeders;

use App\Models\My_Parent;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ParentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('my__parents')->delete();
        $teacher  = [
      
                'email' => 'ahmed@gmail.com',
                'password' => Hash::make("123123123"),
                'Name_Father' => [
                    'fr' => 'ahmed',
                    'ar' => 'أحمد'
                ],
                'National_ID_Father' =>1234567890,
                'Passport_ID_Father' =>1234567890,
                'Phone_Father' =>22222020990,
                'Job_Father' => [
                    'fr' => 'commercant',
                    'ar' => 'تاجر'
                ],
                'Nationality_Father_id' =>140,
                'Blood_Type_Father_id' =>1,
                'Religion_Father_id' =>1,
                'Address_Father' =>'arafat',
                'Name_Mother' => [
                    'fr' => 'cheriva',
                    'ar' => 'شريفة'
                ],
                'National_ID_Mother' =>1234567890,
                'Passport_ID_Mother' =>1234567890,
                'Phone_Mother' =>22246311570,
                'Job_Mother' => [
                    'fr' => 'commercante',
                    'ar' => 'تاجرة'
                ],
                'Nationality_Mother_id' =>140,
                'Blood_Type_Mother_id' =>1,
                'Religion_Mother_id' =>1,
                'Address_Mother' =>'arafat',
        ];

        

        
            My_Parent::create($teacher);
       
    }
}
