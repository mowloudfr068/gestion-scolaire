<?php

namespace Database\Seeders;

use App\Models\Classroom;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassroomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classrooms')->delete();
        $classroomsTable  = [
            [
                'Name' => [
                'fr' => '1AF',
                'ar' => '1AF'
                ],
                'grade_id' =>1,
            ],
            [
                'Name' => [
                    'fr' => '2AF',
                    'ar' => '2AF',
                    ],
                    'grade_id' =>1,
                
            ],
            [
                'Name' => [
                    'fr' => '3AF',
                    'ar' => '3AF',
                    ],
                    'grade_id' =>1,
            ],
        ];

        foreach($classroomsTable as $classroom){
            Classroom::create($classroom);
        }
    }
}
