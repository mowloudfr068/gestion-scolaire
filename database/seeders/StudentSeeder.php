<?php

namespace Database\Seeders;

use App\Models\Student;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->delete();
        $studentsTable  = [
            [
                'name' => [
                    'fr' => 'mowloud',
                    'ar' => 'مولود'
                    ],
                'email' => 'mowloud@gmail.com',
                'password' => Hash::make("123123123"),
                'gender_id' =>1,
                'nationalitie_id' =>140,
                'blood_id' =>1,
                'Date_Birth' =>now(),
                'Grade_id' =>1,
                'Classroom_id' =>1,
                'section_id' =>1,
                'parent_id' =>1,
                'academic_year' =>'2023',
            ],
            [
                'name' => [
                    'fr' => 'moussab',
                    'ar' => 'مصعب'
                    ],
                'email' => 'moussab@gmail.com',
                'password' => Hash::make("123123123"),
                'gender_id' =>1,
                'nationalitie_id' =>140,
                'blood_id' =>1,
                'Date_Birth' =>now(),
                'Grade_id' =>1,
                'Classroom_id' =>1,
                'section_id' =>1,
                'parent_id' =>1,
                'academic_year' =>'2023',
            ],
            [
                'name' => [
                    'fr' => 'fatimetou',
                    'ar' => 'فاطمة'
                    ],
                'email' => 'fatimetou@gmail.com',
                'password' => Hash::make("123123123"),
                'gender_id' =>2,
                'nationalitie_id' =>140,
                'blood_id' =>1,
                'Date_Birth' =>now(),
                'Grade_id' =>1,
                'Classroom_id' =>1,
                'section_id' =>1,
                'parent_id' =>1,
                'academic_year' =>'2023',
            ],
        ];

        

        foreach($studentsTable as $student){
            Student::create($student);
            
        }
    }
}
