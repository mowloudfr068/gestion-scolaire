<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Religion;
class ReligionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('religions')->delete();

        $religions = [

            [
                'fr'=> 'Muslim',
                'ar'=> 'مسلم'
            ],
            [
                'fr'=> 'Christian',
                'ar'=> 'مسيحي'
            ],
            [
                'fr'=> 'Other',
                'ar'=> 'غيرذلك'
            ],

        ];

        foreach ($religions as $R) {
            Religion::create(['Name' => $R]);
        }
    }
}
