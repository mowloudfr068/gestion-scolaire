<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $user  = [
            
                'name' => 'mowloud',
                'email' => 'mowloud@gmail.com',
                'email_verified_at' =>null,
                'password' => Hash::make("123123123"),
                'remember_token' =>null,
        ];

        

        
            User::create($user);
    }
}
