<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Models\Nationalitie;
class NationalitieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nationalities')->delete();

        $nationals = [

            [
                'fr'=> 'Afghan',
                'ar'=> 'أفغانستاني'
            ],
            [

                'fr'=> 'Albanian',
                'ar'=> 'ألباني'
            ],
            [

                'fr'=> 'Aland Islander',
                'ar'=> 'آلاندي'
            ],
            [

                'fr'=> 'Algerian',
                'ar'=> 'جزائري'
            ],
            [

                'fr'=> 'American Samoan',
                'ar'=> 'أمريكي سامواني'
            ],
            [

                'fr'=> 'Andorran',
                'ar'=> 'أندوري'
            ],
            [

                'fr'=> 'Angolan',
                'ar'=> 'أنقولي'
            ],
            [

                'fr'=> 'Anguillan',
                'ar'=> 'أنغويلي'
            ],
            [

                'fr'=> 'Antarctican',
                'ar'=> 'أنتاركتيكي'
            ],
            [

                'fr'=> 'Antiguan',
                'ar'=> 'بربودي'
            ],
            [

                'fr'=> 'Argfrtinian',
                'ar'=> 'أرجنتيني'
            ],
            [

                'fr'=> 'Armfrian',
                'ar'=> 'أرميني'
            ],
            [

                'fr'=> 'Aruban',
                'ar'=> 'أوروبهيني'
            ],
            [

                'fr'=> 'Australian',
                'ar'=> 'أسترالي'
            ],
            [

                'fr'=> 'Austrian',
                'ar'=> 'نمساوي'
            ],
            [

                'fr'=> 'Azerbaijani',
                'ar'=> 'أذربيجاني'
            ],
            [

                'fr'=> 'Bahamian',
                'ar'=> 'باهاميسي'
            ],
            [

                'fr'=> 'Bahraini',
                'ar'=> 'بحريني'
            ],
            [

                'fr'=> 'Bangladeshi',
                'ar'=> 'بنغلاديشي'
            ],
            [

                'fr'=> 'Barbadian',
                'ar'=> 'بربادوسي'
            ],
            [

                'fr'=> 'Belarusian',
                'ar'=> 'روسي'
            ],
            [

                'fr'=> 'Belgian',
                'ar'=> 'بلجيكي'
            ],
            [

                'fr'=> 'Belizean',
                'ar'=> 'بيليزي'
            ],
            [

                'fr'=> 'Bfrinese',
                'ar'=> 'بنيني'
            ],
            [

                'fr'=> 'Saint Barthelmian',
                'ar'=> 'سان بارتيلمي'
            ],
            [

                'fr'=> 'Bermudan',
                'ar'=> 'برمودي'
            ],
            [

                'fr'=> 'Bhutanese',
                'ar'=> 'بوتاني'
            ],
            [

                'fr'=> 'Bolivian',
                'ar'=> 'بوليفي'
            ],
            [

                'fr'=> 'Bosnian / Herzegovinian',
                'ar'=> 'بوسني/هرسكي'
            ],
            [

                'fr'=> 'Botswanan',
                'ar'=> 'بوتسواني'
            ],
            [

                'fr'=> 'Bouvetian',
                'ar'=> 'بوفيهي'
            ],
            [

                'fr'=> 'Brazilian',
                'ar'=> 'برازيلي'
            ],
            [

                'fr'=> 'British Indian Ocean Territory',
                'ar'=> 'إقليم المحيط الهندي البريطاني'
            ],
            [

                'fr'=> 'Bruneian',
                'ar'=> 'بروني'
            ],
            [

                'fr'=> 'Bulgarian',
                'ar'=> 'بلغاري'
            ],
            [

                'fr'=> 'Burkinabe',
                'ar'=> 'بوركيني'
            ],
            [

                'fr'=> 'Burundian',
                'ar'=> 'بورونيدي'
            ],
            [

                'fr'=> 'Cambodian',
                'ar'=> 'كمبودي'
            ],
            [

                'fr'=> 'Cameroonian',
                'ar'=> 'كاميروني'
            ],
            [

                'fr'=> 'Canadian',
                'ar'=> 'كندي'
            ],
            [

                'fr'=> 'Cape Verdean',
                'ar'=> 'الرأس الأخضر'
            ],
            [

                'fr'=> 'Caymanian',
                'ar'=> 'كايماني'
            ],
            [

                'fr'=> 'Cfrtral African',
                'ar'=> 'أفريقي'
            ],
            [

                'fr'=> 'Chadian',
                'ar'=> 'تشادي'
            ],
            [

                'fr'=> 'Chilean',
                'ar'=> 'شيلي'
            ],
            [

                'fr'=> 'Chinese',
                'ar'=> 'صيني'
            ],
            [

                'fr'=> 'Christmas Islander',
                'ar'=> 'جزيرة عيد الميلاد'
            ],
            [

                'fr'=> 'Cocos Islander',
                'ar'=> 'جزر كوكوس'
            ],
            [

                'fr'=> 'Colombian',
                'ar'=> 'كولومبي'
            ],
            [

                'fr'=> 'Comorian',
                'ar'=> 'جزر القمر'
            ],
            [

                'fr'=> 'Congolese',
                'ar'=> 'كونغي'
            ],
            [

                'fr'=> 'Cook Islander',
                'ar'=> 'جزر كوك'
            ],
            [

                'fr'=> 'Costa Rican',
                'ar'=> 'كوستاريكي'
            ],
            [

                'fr'=> 'Croatian',
                'ar'=> 'كوراتي'
            ],
            [

                'fr'=> 'Cuban',
                'ar'=> 'كوبي'
            ],
            [

                'fr'=> 'Cypriot',
                'ar'=> 'قبرصي'
            ],
            [

                'fr'=> 'Curacian',
                'ar'=> 'كوراساوي'
            ],
            [

                'fr'=> 'Czech',
                'ar'=> 'تشيكي'
            ],
            [

                'fr'=> 'Danish',
                'ar'=> 'دنماركي'
            ],
            [

                'fr'=> 'Djiboutian',
                'ar'=> 'جيبوتي'
            ],
            [

                'fr'=> 'Dominican',
                'ar'=> 'دومينيكي'
            ],
            [

                'fr'=> 'Dominican',
                'ar'=> 'دومينيكي'
            ],
            [

                'fr'=> 'Ecuadorian',
                'ar'=> 'إكوادوري'
            ],
            [

                'fr'=> 'Egyptian',
                'ar'=> 'مصري'
            ],
            [

                'fr'=> 'Salvadoran',
                'ar'=> 'سلفادوري'
            ],
            [

                'fr'=> 'Equatorial Guinean',
                'ar'=> 'غيني'
            ],
            [

                'fr'=> 'Eritrean',
                'ar'=> 'إريتيري'
            ],
            [

                'fr'=> 'Estonian',
                'ar'=> 'استوني'
            ],
            [

                'fr'=> 'Ethiopian',
                'ar'=> 'أثيوبي'
            ],
            [

                'fr'=> 'Falkland Islander',
                'ar'=> 'فوكلاندي'
            ],
            [

                'fr'=> 'Faroese',
                'ar'=> 'جزر فارو'
            ],
            [

                'fr'=> 'Fijian',
                'ar'=> 'فيجي'
            ],
            [

                'fr'=> 'Finnish',
                'ar'=> 'فنلندي'
            ],
            [

                'fr'=> 'Frfrch',
                'ar'=> 'فرنسي'
            ],
            [

                'fr'=> 'Frfrch Guianese',
                'ar'=> 'غويانا الفرنسية'
            ],
            [

                'fr'=> 'Frfrch Polynesian',
                'ar'=> 'بولينيزيي'
            ],
            [

                'fr'=> 'Frfrch',
                'ar'=> 'أراض فرنسية جنوبية وأنتارتيكية'
            ],
            [

                'fr'=> 'Gabonese',
                'ar'=> 'غابوني'
            ],
            [

                'fr'=> 'Gambian',
                'ar'=> 'غامبي'
            ],
            [

                'fr'=> 'Georgian',
                'ar'=> 'جيورجي'
            ],
            [

                'fr'=> 'German',
                'ar'=> 'ألماني'
            ],
            [

                'fr'=> 'Ghanaian',
                'ar'=> 'غاني'
            ],
            [

                'fr'=> 'Gibraltar',
                'ar'=> 'جبل طارق'
            ],
            [

                'fr'=> 'Guernsian',
                'ar'=> 'غيرنزي'
            ],
            [

                'fr'=> 'Greek',
                'ar'=> 'يوناني'
            ],
            [

                'fr'=> 'Grefrlandic',
                'ar'=> 'جرينلاندي'
            ],
            [

                'fr'=> 'Grfradian',
                'ar'=> 'غرينادي'
            ],
            [

                'fr'=> 'Guadeloupe',
                'ar'=> 'جزر جوادلوب'
            ],
            [

                'fr'=> 'Guamanian',
                'ar'=> 'جوامي'
            ],
            [

                'fr'=> 'Guatemalan',
                'ar'=> 'غواتيمالي'
            ],
            [

                'fr'=> 'Guinean',
                'ar'=> 'غيني'
            ],
            [

                'fr'=> 'Guinea-Bissauan',
                'ar'=> 'غيني'
            ],
            [

                'fr'=> 'Guyanese',
                'ar'=> 'غياني'
            ],
            [

                'fr'=> 'Haitian',
                'ar'=> 'هايتي'
            ],
            [

                'fr'=> 'Heard and Mc Donald Islanders',
                'ar'=> 'جزيرة هيرد وجزر ماكدونالد'
            ],
            [

                'fr'=> 'Honduran',
                'ar'=> 'هندوراسي'
            ],
            [

                'fr'=> 'Hongkongese',
                'ar'=> 'هونغ كونغي'
            ],
            [

                'fr'=> 'Hungarian',
                'ar'=> 'مجري'
            ],
            [

                'fr'=> 'Icelandic',
                'ar'=> 'آيسلندي'
            ],
            [

                'fr'=> 'Indian',
                'ar'=> 'هندي'
            ],
            [

                'fr'=> 'Manx',
                'ar'=> 'ماني'
            ],
            [

                'fr'=> 'Indonesian',
                'ar'=> 'أندونيسيي'
            ],
            [

                'fr'=> 'Iranian',
                'ar'=> 'إيراني'
            ],
            [

                'fr'=> 'Iraqi',
                'ar'=> 'عراقي'
            ],
            [

                'fr'=> 'Irish',
                'ar'=> 'إيرلندي'
            ],
            [

                'fr'=> 'Italian',
                'ar'=> 'إيطالي'
            ],
            [

                'fr'=> 'Ivory Coastian',
                'ar'=> 'ساحل العاج'
            ],
            [

                'fr'=> 'Jersian',
                'ar'=> 'جيرزي'
            ],
            [

                'fr'=> 'Jamaican',
                'ar'=> 'جمايكي'
            ],
            [

                'fr'=> 'Japanese',
                'ar'=> 'ياباني'
            ],
            [

                'fr'=> 'Jordanian',
                'ar'=> 'أردني'
            ],
            [

                'fr'=> 'Kazakh',
                'ar'=> 'كازاخستاني'
            ],
            [

                'fr'=> 'Kfryan',
                'ar'=> 'كيني'
            ],
            [

                'fr'=> 'I-Kiribati',
                'ar'=> 'كيريباتي'
            ],
            [

                'fr'=> 'North Korean',
                'ar'=> 'كوري'
            ],
            [

                'fr'=> 'South Korean',
                'ar'=> 'كوري'
            ],
            [

                'fr'=> 'Kosovar',
                'ar'=> 'كوسيفي'
            ],
            [

                'fr'=> 'Kuwaiti',
                'ar'=> 'كويتي'
            ],
            [

                'fr'=> 'Kyrgyzstani',
                'ar'=> 'قيرغيزستاني'
            ],
            [

                'fr'=> 'Laotian',
                'ar'=> 'لاوسي'
            ],
            [

                'fr'=> 'Latvian',
                'ar'=> 'لاتيفي'
            ],
            [

                'fr'=> 'Lebanese',
                'ar'=> 'لبناني'
            ],
            [

                'fr'=> 'Basotho',
                'ar'=> 'ليوسيتي'
            ],
            [

                'fr'=> 'Liberian',
                'ar'=> 'ليبيري'
            ],
            [

                'fr'=> 'Libyan',
                'ar'=> 'ليبي'
            ],
            [

                'fr'=> 'Liechtfrstein',
                'ar'=> 'ليختنشتيني'
            ],
            [

                'fr'=> 'Lithuanian',
                'ar'=> 'لتوانيي'
            ],
            [

                'fr'=> 'Luxembourger',
                'ar'=> 'لوكسمبورغي'
            ],
            [

                'fr'=> 'Sri Lankian',
                'ar'=> 'سريلانكي'
            ],
            [

                'fr'=> 'Macanese',
                'ar'=> 'ماكاوي'
            ],
            [

                'fr'=> 'Macedonian',
                'ar'=> 'مقدوني'
            ],
            [

                'fr'=> 'Malagasy',
                'ar'=> 'مدغشقري'
            ],
            [

                'fr'=> 'Malawian',
                'ar'=> 'مالاوي'
            ],
            [

                'fr'=> 'Malaysian',
                'ar'=> 'ماليزي'
            ],
            [

                'fr'=> 'Maldivian',
                'ar'=> 'مالديفي'
            ],
            [

                'fr'=> 'Malian',
                'ar'=> 'مالي'
            ],
            [

                'fr'=> 'Maltese',
                'ar'=> 'مالطي'
            ],
            [

                'fr'=> 'Marshallese',
                'ar'=> 'مارشالي'
            ],
            [

                'fr'=> 'Martiniquais',
                'ar'=> 'مارتينيكي'
            ],
            [

                'fr'=> 'Mauritanian',
                'ar'=> 'موريتانيي'
            ],
            [

                'fr'=> 'Mauritian',
                'ar'=> 'موريشيوسي'
            ],
            [

                'fr'=> 'Mahoran',
                'ar'=> 'مايوتي'
            ],
            [

                'fr'=> 'Mexican',
                'ar'=> 'مكسيكي'
            ],
            [

                'fr'=> 'Micronesian',
                'ar'=> 'مايكرونيزيي'
            ],
            [

                'fr'=> 'Moldovan',
                'ar'=> 'مولديفي'
            ],
            [

                'fr'=> 'Monacan',
                'ar'=> 'مونيكي'
            ],
            [

                'fr'=> 'Mongolian',
                'ar'=> 'منغولي'
            ],
            [

                'fr'=> 'Montfregrin',
                'ar'=> 'الجبل الأسود'
            ],
            [

                'fr'=> 'Montserratian',
                'ar'=> 'مونتسيراتي'
            ],
            [

                'fr'=> 'Moroccan',
                'ar'=> 'مغربي'
            ],
            [

                'fr'=> 'Mozambican',
                'ar'=> 'موزمبيقي'
            ],
            [

                'fr'=> 'Myanmarian',
                'ar'=> 'ميانماري'
            ],
            [

                'fr'=> 'Namibian',
                'ar'=> 'ناميبي'
            ],
            [

                'fr'=> 'Nauruan',
                'ar'=> 'نوري'
            ],
            [

                'fr'=> 'Nepalese',
                'ar'=> 'نيبالي'
            ],
            [

                'fr'=> 'Dutch',
                'ar'=> 'هولندي'
            ],
            [

                'fr'=> 'Dutch Antilier',
                'ar'=> 'هولندي'
            ],
            [

                'fr'=> 'New Caledonian',
                'ar'=> 'كاليدوني'
            ],
            [

                'fr'=> 'New Zealander',
                'ar'=> 'نيوزيلندي'
            ],
            [

                'fr'=> 'Nicaraguan',
                'ar'=> 'نيكاراجوي'
            ],
            [

                'fr'=> 'Nigerifr',
                'ar'=> 'نيجيري'
            ],
            [

                'fr'=> 'Nigerian',
                'ar'=> 'نيجيري'
            ],
            [

                'fr'=> 'Niuean',
                'ar'=> 'ني'
            ],
            [

                'fr'=> 'Norfolk Islander',
                'ar'=> 'نورفوليكي'
            ],
            [

                'fr'=> 'Northern Marianan',
                'ar'=> 'ماريني'
            ],
            [

                'fr'=> 'Norwegian',
                'ar'=> 'نرويجي'
            ],
            [

                'fr'=> 'Omani',
                'ar'=> 'عماني'
            ],
            [

                'fr'=> 'Pakistani',
                'ar'=> 'باكستاني'
            ],
            [

                'fr'=> 'Palauan',
                'ar'=> 'بالاوي'
            ],
            [

                'fr'=> 'Palestinian',
                'ar'=> 'فلسطيني'
            ],
            [

                'fr'=> 'Panamanian',
                'ar'=> 'بنمي'
            ],
            [

                'fr'=> 'Papua New Guinean',
                'ar'=> 'بابوي'
            ],
            [

                'fr'=> 'Paraguayan',
                'ar'=> 'بارغاوي'
            ],
            [

                'fr'=> 'Peruvian',
                'ar'=> 'بيري'
            ],
            [

                'fr'=> 'Filipino',
                'ar'=> 'فلبيني'
            ],
            [

                'fr'=> 'Pitcairn Islander',
                'ar'=> 'بيتكيرني'
            ],
            [

                'fr'=> 'Polish',
                'ar'=> 'بوليني'
            ],
            [

                'fr'=> 'Portuguese',
                'ar'=> 'برتغالي'
            ],
            [

                'fr'=> 'Puerto Rican',
                'ar'=> 'بورتي'
            ],
            [

                'fr'=> 'Qatari',
                'ar'=> 'قطري'
            ],
            [

                'fr'=> 'Reunionese',
                'ar'=> 'ريونيوني'
            ],
            [

                'fr'=> 'Romanian',
                'ar'=> 'روماني'
            ],
            [

                'fr'=> 'Russian',
                'ar'=> 'روسي'
            ],
            [

                'fr'=> 'Rwandan',
                'ar'=> 'رواندا'
            ],
            [

                'fr'=> '',
                'ar'=> 'Kittitian/Nevisian'
            ],
            [

                'fr'=> 'St. Martian(Frfrch)',
                'ar'=> 'ساينت مارتني فرنسي'
            ],
            [

                'fr'=> 'St. Martian(Dutch)',
                'ar'=> 'ساينت مارتني هولندي'
            ],
            [

                'fr'=> 'St. Pierre and Miquelon',
                'ar'=> 'سان بيير وميكلوني'
            ],
            [

                'fr'=> 'Saint Vincfrt and the Grfradines',
                'ar'=> 'سانت فنسنت وجزر غرينادين'
            ],
            [

                'fr'=> 'Samoan',
                'ar'=> 'ساموي'
            ],
            [

                'fr'=> 'Sammarinese',
                'ar'=> 'ماريني'
            ],
            [

                'fr'=> 'Sao Tomean',
                'ar'=> 'ساو تومي وبرينسيبي'
            ],
            [

                'fr'=> 'Saudi Arabian',
                'ar'=> 'سعودي'
            ],
            [

                'fr'=> 'Sfregalese',
                'ar'=> 'سنغالي'
            ],
            [

                'fr'=> 'Serbian',
                'ar'=> 'صربي'
            ],
            [

                'fr'=> 'Seychellois',
                'ar'=> 'سيشيلي'
            ],
            [

                'fr'=> 'Sierra Leonean',
                'ar'=> 'سيراليوني'
            ],
            [

                'fr'=> 'Singaporean',
                'ar'=> 'سنغافوري'
            ],
            [

                'fr'=> 'Slovak',
                'ar'=> 'سولفاكي'
            ],
            [

                'fr'=> 'Slovfrian',
                'ar'=> 'سولفيني'
            ],
            [

                'fr'=> 'Solomon Island',
                'ar'=> 'جزر سليمان'
            ],
            [

                'fr'=> 'Somali',
                'ar'=> 'صومالي'
            ],
            [

                'fr'=> 'South African',
                'ar'=> 'أفريقي'
            ],
            [

                'fr'=> 'South Georgia and the South Sandwich',
                'ar'=> 'لمنطقة القطبية الجنوبية'
            ],
            [

                'fr'=> 'South Sudanese',
                'ar'=> 'سوادني جنوبي'
            ],
            [

                'fr'=> 'Spanish',
                'ar'=> 'إسباني'
            ],
            [

                'fr'=> 'St. Helfrian',
                'ar'=> 'هيلاني'
            ],
            [

                'fr'=> 'Sudanese',
                'ar'=> 'سوداني'
            ],
            [

                'fr'=> 'Surinamese',
                'ar'=> 'سورينامي'
            ],
            [

                'fr'=> 'Svalbardian/Jan Mayfrian',
                'ar'=> 'سفالبارد ويان ماين'
            ],
            [

                'fr'=> 'Swazi',
                'ar'=> 'سوازيلندي'
            ],
            [

                'fr'=> 'Swedish',
                'ar'=> 'سويدي'
            ],
            [

                'fr'=> 'Swiss',
                'ar'=> 'سويسري'
            ],
            [

                'fr'=> 'Syrian',
                'ar'=> 'سوري'
            ],
            [

                'fr'=> 'Taiwanese',
                'ar'=> 'تايواني'
            ],
            [

                'fr'=> 'Tajikistani',
                'ar'=> 'طاجيكستاني'
            ],
            [

                'fr'=> 'Tanzanian',
                'ar'=> 'تنزانيي'
            ],
            [

                'fr'=> 'Thai',
                'ar'=> 'تايلندي'
            ],
            [

                'fr'=> 'Timor-Lestian',
                'ar'=> 'تيموري'
            ],
            [

                'fr'=> 'Togolese',
                'ar'=> 'توغي'
            ],
            [

                'fr'=> 'Tokelaian',
                'ar'=> 'توكيلاوي'
            ],
            [

                'fr'=> 'Tongan',
                'ar'=> 'تونغي'
            ],
            [

                'fr'=> 'Trinidadian/Tobagonian',
                'ar'=> 'ترينيداد وتوباغو'
            ],
            [

                'fr'=> 'Tunisian',
                'ar'=> 'تونسي'
            ],
            [

                'fr'=> 'Turkish',
                'ar'=> 'تركي'
            ],
            [

                'fr'=> 'Turkmfr',
                'ar'=> 'تركمانستاني'
            ],
            [

                'fr'=> 'Turks and Caicos Islands',
                'ar'=> 'جزر توركس وكايكوس'
            ],
            [

                'fr'=> 'Tuvaluan',
                'ar'=> 'توفالي'
            ],
            [

                'fr'=> 'Ugandan',
                'ar'=> 'أوغندي'
            ],
            [

                'fr'=> 'Ukrainian',
                'ar'=> 'أوكراني'
            ],
            [

                'fr'=> 'Emirati',
                'ar'=> 'إماراتي'
            ],
            [

                'fr'=> 'British',
                'ar'=> 'بريطاني'
            ],
            [

                'fr'=> 'American',
                'ar'=> 'أمريكي'
            ],
            [

                'fr'=> 'US Minor Outlying Islander',
                'ar'=> 'أمريكي'
            ],
            [

                'fr'=> 'Uruguayan',
                'ar'=> 'أورغواي'
            ],
            [

                'fr'=> 'Uzbek',
                'ar'=> 'أوزباكستاني'
            ],
            [

                'fr'=> 'Vanuatuan',
                'ar'=> 'فانواتي'
            ],
            [

                'fr'=> 'Vfrezuelan',
                'ar'=> 'فنزويلي'
            ],
            [

                'fr'=> 'Vietnamese',
                'ar'=> 'فيتنامي'
            ],
            [

                'fr'=> 'American Virgin Islander',
                'ar'=> 'أمريكي'
            ],
            [

                'fr'=> 'Vatican',
                'ar'=> 'فاتيكاني'
            ],
            [

                'fr'=> 'Wallisian/Futunan',
                'ar'=> 'فوتوني'
            ],
            [

                'fr'=> 'Sahrawian',
                'ar'=> 'صحراوي'
            ],
            [

                'fr'=> 'Yemfri',
                'ar'=> 'يمني'
            ],
            [

                'fr'=> 'Zambian',
                'ar'=> 'زامبياني'
            ],
            [

                'fr'=> 'Zimbabwean',
                'ar'=> 'زمبابوي'
            ]
        ];

        foreach ($nationals as $n) {
            Nationalitie::create(['Name' => $n]);
        }
    }
}
