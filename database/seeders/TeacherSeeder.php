<?php

namespace Database\Seeders;

use App\Models\Teacher;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teachers')->delete();
        $teachersTable  = [
            [
                'email' => 'mowloud@gmail.com',
                'password' => Hash::make("123123123"),
                'Name' => [
                'fr' => 'mowloud',
                'ar' => 'مولود'
                ],
                'Specialization_id' =>2,
                'Gender_id' =>1,
                'Joining_Date' =>now(),
                'Address' => 'arafat',
            ],
            [
                'email' => 'ahmed@gmail.com',
                'password' => Hash::make("123123123"),
                'Name' => [
                'fr' => 'ahmed',
                'ar' => 'أحمد'
                ],
                'Specialization_id' =>3,
                'Gender_id' =>1,
                'Joining_Date' =>now(),
                'Address' => 'arafat',
                
            ],
            [
                'email' => 'salma@gmail.com',
                'password' => Hash::make("123123123"),
                'Name' => [
                'fr' => 'salma',
                'ar' => 'السالمة'
                ],
                'Specialization_id' =>2,
                'Gender_id' =>1,
                'Joining_Date' =>now(),
                'Address' => 'arafat',
            ],
        ];

        

        foreach($teachersTable as $teacher){
            Teacher::create($teacher);
            
        }
    }
}
