<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\TypeBlood;

class TypeBloodSeeder extends Seeder
{

    public function run()
    {
        DB::table('type_bloods')->delete();
        $bgs = ['O-', 'O+', 'A+', 'A-', 'B+', 'B-', 'AB+', 'AB-'];

        foreach($bgs as  $bg){
            TypeBlood::create(['Name' => $bg]);
        }

    }
}
