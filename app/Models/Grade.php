<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Grade extends Model
{
    use HasFactory;
    use HasTranslations;
    public $translatable = ['Name'];
    protected $table = 'grades';
    protected $fillable = ['Name', 'Notes'];
    public $timestamps = true;

    // relation de grades pour obtient les sections quil lui appartient
    public function sections()
    {
        return $this->hasMany('App\Models\Section', 'grade_id');
    }
}
