<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Classroom extends Model
{
    use HasTranslations;
    public $translatable = ['Name'];
    use HasFactory;
    protected $table = 'classrooms';
    public $timestamps = true;
    protected $fillable = ['Name', 'grade_id'];

    public function grade()
    {
        return $this->belongsTo('App\Models\Grade', 'grade_id');
    }
}
