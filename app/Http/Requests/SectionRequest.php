<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SectionRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'Name_ar' => 'required',
            'Name_fr' => 'required',
            'grade_id' => 'required',
            'class_id' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'Name_ar' => __('validation.required'),
            'Name_fr' => __('validation.required'),
            'grade_id.required' => __('validation.required'),
            'class_id.required' => __('validation.required'),
        ];
    }
}
