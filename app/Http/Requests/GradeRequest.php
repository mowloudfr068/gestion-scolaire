<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GradeRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'Name_fr' => 'required|unique:grades,name->fr,'.$this->id,
            'Name_ar' => 'required|unique:grades,name->ar,'.$this->id,
        ];
    }

    public function messages()
    {
        return [
            'Name_ar.required' => __('validation.required'),
            'Name_ar.unique' => __('validation.unique'),
            'Name_fr.required' => __('validation.required'),
            'Name_fr.unique' => __('validation.unique'),
        ];
    }
}
