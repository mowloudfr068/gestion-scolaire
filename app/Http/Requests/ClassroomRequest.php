<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClassroomRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'list_classes.*.Name_ar' => 'required|unique:classrooms,Name->ar' . $this->id,
                        'list_classes.*.Name_fr' => 'required|unique:classrooms,Name->fr' . $this->id,
                        'list_classes.*.grade_id' => 'required|numeric',
                    ];
                }
            case 'PUT':
            case 'PATCH': {
                    return [
                        'Name_ar' => 'required|unique:classrooms,Name->ar',
                        'Name_fr' => 'required|unique:classrooms,Name->fr',
                        'grade_id' => 'required|numeric',
                    ];
                }
            default:
        }
    }

    public function messages()
    {
        return [
            'Name_ar.required' =>  __('validation.required'),
            'Name_fr.required' =>  __('validation.required'),
            'grade_id.required' => __('validation.required'),
            'grade_id.numeric' =>  __('validation.numeric'),
        ];
    }
}
