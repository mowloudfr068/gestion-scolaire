<?php

namespace App\Http\Controllers\Sections;

use App\Http\Controllers\Controller;
use App\Http\Requests\SectionRequest;
use App\Models\Classroom;
use App\Models\Grade;
use App\Models\Section;
use App\Models\Teacher;
use Illuminate\Http\Request;

class SectionController extends Controller
{

    public function index()
    {
        $grades = Grade::with(['sections'])->get();
        $list_grades = Grade::all();
        $teachers = Teacher::all();
        return view('pages.sections.index', compact('grades', 'list_grades','teachers'));
    }


    public function create()
    {
        //
    }


    public function store(SectionRequest $request)
    {
        // return $request->teacher_id; 
        try {

            $validated = $request->validated();
            $section = new Section();
            $section->Name = ['ar' => $request->Name_ar, 'fr' => $request->Name_fr];
            $section->grade_id = $request->grade_id;
            $section->classroom_id = $request->class_id;
            $section->status = 1;
            $section->save();
            $section->teachers()->attach($request->teacher_id);
            toastr()->success(__('messages.success'));

            return redirect()->route('sections.index');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }


    public function show(Section $section)
    {
        //
    }


    public function edit(Section $section)
    {
        //
    }


    public function update(SectionRequest $request)
    {
        try {
            $validated = $request->validated();
            $section = Section::findOrFail($request->id);

            $section->Name = ['ar' => $request->Name_ar, 'fr' => $request->Name_fr];
            $section->grade_id = $request->grade_id;
            $section->classroom_id = $request->class_id;

            if (isset($request->Status)) {
                $section->Status = 1;
            } else {
                $section->Status = 2;
            }

            // update pivot tABLE
        if (isset($request->teacher_id)) {
            $section->teachers()->sync($request->teacher_id);
        } else {
            $section->teachers()->sync(array());
        }
            $section->save();
            toastr()->success(trans('messages.update'));

            return redirect()->route('Sections.index');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }


    public function destroy(Request $request)
    {
        Section::findOrFail($request->id)->delete();
        toastr()->error(trans('messages.delete'));
        return redirect()->route('sections.index');
    }

    public function getClassrooms($id)
    {
        $classes = Classroom::where("grade_id", $id)->pluck("Name", "id");
        return $classes;
    }
}
