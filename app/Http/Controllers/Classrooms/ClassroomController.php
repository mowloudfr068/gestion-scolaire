<?php

namespace App\Http\Controllers\Classrooms;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClassroomRequest;
use App\Models\Classroom;
use App\Models\Grade;
use Illuminate\Http\Request;

class ClassroomController extends Controller
{

    public function index()
    {
        $classes = Classroom::all();
        $grades = Grade::all();
        return view('pages.classes.index', compact('classes', 'grades'));
    }


    public function create()
    {
        //
    }


    public function store(ClassroomRequest $request)
    {
        // return $request;
        $list_classes = $request->list_classes;

        try {

            // $validated = $request->validated();
            foreach ($list_classes as $list_class) {

                $classe = new Classroom();

                $classe->Name = ['fr' => $list_class['Name_fr'], 'ar' => $list_class['Name_ar']];

                $classe->grade_id = $list_class['grade_id'];

                $classe->save();
            }

            toastr()->success(trans('messages.success'));
            return redirect()->route('classrooms.index');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }


    public function show(Classroom $classroom)
    {
        //
    }


    public function edit(Classroom $classroom)
    {
        //
    }

    public function update(ClassroomRequest $request)
    {
        // return $request;
        try {

            $classrooms = Classroom::findOrFail($request->id);

            $classrooms->update([

                $classrooms->Name = ['ar' => $request->Name_ar, 'fr' => $request->Name_fr],
                $classrooms->grade_id = $request->grade_id,
            ]);
            toastr()->success(trans('messages.update'));
            return redirect()->route('classrooms.index');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }


    public function destroy(Request $request)
    {

        $classe = Classroom::findOrFail($request->id)->delete();
        toastr()->error(trans('messages.delete'));
        return redirect()->route('classrooms.index');
    }

    public function delete_all(Request $request)
    {
        //explode: transformer les id sous array au lieu de "id1,id2,id3..."
        $delete_all_id = explode(",", $request->delete_all_id);
        Classroom::whereIn('id', $delete_all_id)->delete();
        toastr()->error(__('messages.delete'));
        return redirect()->route('classrooms.index');
    }

    public function filter_classes(Request $request)
    {
        $grades = Grade::all();

        if ($request->grade_id == 'all') {

            return redirect()->route('classrooms.index');
        }
        $classes = Classroom::select('*')->where('grade_id', '=', $request->grade_id)->get();
        return view('pages.classes.index', compact('grades', 'classes'));
    }
}
