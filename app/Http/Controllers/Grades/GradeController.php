<?php

namespace App\Http\Controllers\Grades;

use App\Http\Controllers\Controller;
use App\Http\Requests\GradeRequest;
use App\Models\Classroom;
use App\Models\Grade;
use Illuminate\Http\Request;
use Yoeunes\Toastr\Facades\Toastr;

class GradeController extends Controller
{

    public function index()
    {
        $grades = Grade::all();
        return view('pages.grades.index', compact('grades'));
    }


    public function create()
    {
        //
    }


    public function store(GradeRequest $request)
    {

        try {

            $validated = $request->validated();
            $grade = new Grade();
            /*
              $translations = [
                  'fr' => $request->Name_fr,
                  'ar' => $request->Name_ar
              ];
              $grade->setTranslations('Name', $translations);
              */
            $grade->Name = ['fr' => $request->Name_fr, 'ar' => $request->Name_ar];
            $grade->Notes = $request->Notes;
            $grade->save();
            toastr()->success(__('messages.success'));
            return redirect()->route('grades.index');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }


    public function show(Grade $grade)
    {
        //
    }


    public function edit(Grade $grade)
    {
        //
    }


    public function update(GradeRequest $request)
    {
        try {

            $validated = $request->validated();
            $grade = Grade::findOrFail($request->id);
            $grade->update([
                $grade->Name = ['ar' => $request->Name_ar, 'fr' => $request->Name_fr],
                $grade->Notes = $request->Notes,
            ]);
            toastr()->success(__('messages.update'));
            return redirect()->route('grades.index');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }



    public function destroy(Request $request)
    {
        
        $class_id = Classroom::where('grade_id',$request->id)->pluck('grade_id');

      if($class_id->count() == 0){
        $grade = Grade::findOrFail($request->id)->delete();
        toastr()->error(trans('messages.delete'));
        return redirect()->route('grades.index');
    }

    else{
        toastr()->error(trans('grades_trans.supprimer_Niveau_erreur'));
          return redirect()->route('grades.index');

      }

    }
}
