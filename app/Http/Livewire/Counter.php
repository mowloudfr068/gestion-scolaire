<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class Counter extends Component
{
   public $counter=0;
   public $search;
    public function render()
    {
        return view('livewire.counter',[
            'users' => User::where('name','like','%'.$this->search.'%')->get()
        ]);
    }

    public function increments()
    {
        $this->counter++;
    }

    public function decrements()
    {
        $this->counter--;
    }

   

}
