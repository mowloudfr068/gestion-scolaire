<?php

return [

    'Observation' => 'ملاحظات',
    'enregistre' => 'حفظ البيانات',
    'Opperation' => 'العمليات',
    'Modifier' => 'تعديل',
    'Supprimer' => 'حذف',
    'Annuler' => 'اغلاق',
    'Home' => 'الرئيسية',
    'exist' => 'البيانات موجودة مسبقا!',
    'Tableau De Bord' => 'لوحة التحكم',
    'Choose' => 'اختيار من القائمة',
    
];
