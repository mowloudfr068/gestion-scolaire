<?php
return [

   'Niveau scolaires' => 'المراحل الدراسية',
   'List niveau scolaires' => 'قائمة المراحل الدراسية',
   'Ajouter Niveau' => 'اضافة مرحلة',
   'Modifier Niveau' => 'تعديل مرحلة',
   'supprimer Niveau' => 'حذف مرحلة',
   'Attention Niveau' => 'هل انت متاكد من عملية الحذف ؟',
   'Niveau_nom_ar' => 'اسم المرحلة بالعربية',
   'Niveau_nom_fr' => 'اسم المرحلة بالفرنسية',
   'Nom' => 'اسم المرحلة',
   'supprimer_Niveau_erreur' => 'لايمكن حذف المرحلة بسبب وجود صفوف تابعة لها',
   'Niveau' => 'المراحل',



];
