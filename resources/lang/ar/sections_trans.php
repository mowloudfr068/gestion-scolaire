<?php

return [

    'les Sections' => 'الأقسام الدراسية',
    'Liste_section' => 'قائمة الأقسام الدراسية',
    'Ajouter_section' => 'اضافة قسم',
    'Modifier_section' => 'تعديل قسم',
    'Nom' => 'اسم القسم',
    'Supprimer_section' => 'حذف قسم',
    'Supprimer_lignes' => 'حذف الأقسام المختارة',
    'confirm_supp_sec' => 'هل انت متاكد من عملية الحذف ؟',
    'Veuillez_section_arabe' => 'يرجي ادخال اسم القسم باللغة العربية',
    'Veuillez_section_français' => 'يرجي ادخال اسم القسم باللغة الفرنسية',
    'section_arabe' => 'اسم القسم بالعربية',
    'section_français' => 'اسم القسم بالفرنسية',
    'nom_Niveau' => 'اسم المرحلة',
    'nom_classe' => 'اسم الصف',
    'Voir tous' => 'عرض الكل',
    'activer' => 'مفعل',
    'desactiver' => 'غير مفعل',
    'status' => 'الحالة'




];
