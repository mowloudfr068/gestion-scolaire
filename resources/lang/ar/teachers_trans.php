<?php

return [
    'Teachers'=>'المعلمين',
    'List_Teachers' => 'قائمة المعلمين',
    'Name_Teacher'=>'اسم المعلم',
    'Add_Teacher'=>'اضافة معلم',
    'Edit_Teacher'=>'تعديل معلم',
    'Delete_Teacher'=>'حذف معلم',
    'Email'=>'البريد الالكتروني',
    'Password'=>'كلمة المرور',
    'Name_ar'=>'اسم المعلم باللغة العربية',
    'Name_fr'=>'اسم المعلم باللغة الانجليزية',
    'specialization'=>'التخصص',
    'Gender'=>'النوع',
    'Joining_Date'=>'تاريخ التعين',
    'Address'=>'العنوان',
    'Attention_teacher' => 'هل انت متاكد من عملية الحذف ؟',


];
