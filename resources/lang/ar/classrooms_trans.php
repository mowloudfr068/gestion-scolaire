<?php

return [

    'les classes' => 'الصفوف الدراسية',
    'Liste_classe' => 'قائمة الصفوف الدراسية',
    'Ajouter_classe' => 'اضافة صف',
    'Modifier_classe' => 'تعديل صف',
    'Nom' => 'اسم الصف',
    'Supprimer_classe' => 'حذف صف',
    'Supprimer_lignes' => 'حذف الصفوف المختارة',
    'Recherche_par_scolaire' => 'بحث باسم المرحلة',
    'confirm_supp_cls' => 'هل انت متاكد من عملية الحذف ؟',
    'Veuillez_classe_arabe' => 'يرجي ادخال اسم الصف باللغة العربية',
    'Veuillez_classe_français' => 'يرجي ادخال اسم الصف باللغة الفرنسية',
    'classe_arabe' => 'اسم الصف بالعربية',
    'classe_français' => 'اسم الصف بالفرنسية',
    'nom_Niveau' => 'اسم المرحلة',
    'Ajouter_ligne' => 'ادراج سجل',
    'Supprimer_ligne' => 'حذف سطر	',
    'supprimer_ligne_contient_classes' => 'لا يمكن حذف المرحلة لانها تحتوي علي صفوف',
    'suprimer_checked' => 'حذف الصفوف المختارة',
    'Voir tous' => 'عرض الكل',
    'chercher par Niveau' => 'بحث بواسطة المرحلة',



];
