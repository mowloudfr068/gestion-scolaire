<?php

return [

    'les classes' => 'les classes',
    'Liste_classe' => 'Liste de classe',
    'Ajouter_classe' => 'Ajouter une classe',
    'Modifier_classe' => 'Modifier une classe',
    'Nom' => 'Nom',
    'Supprimer_classe' => 'Supprimer une classe',
    'Supprimer_lignes' => 'Supprimer les lignes sélectionnées',
    'Recherche_par_scolaire' => 'Recherche par niveau scolaire',
    'confirm_supp_cls' => 'ete vous sur pour la suppression ?',
    'Veuillez_classe_arabe' => 'Veuillez entrer le nom de la classe en arabe',
    'Veuillez_classe_français' => 'Veuillez entrer le nom de la classe en français',
    'classe_arabe' => 'nom en arabe',
    'classe_français' => 'nom en français',
    'nom_Niveau' => 'le nom du Niveau',
    'Ajouter_ligne' => 'Ajouter ligne',
    'Supprimer_ligne' => 'Supprimer ligne',
    'supprimer_ligne_contient_classes' => 'Il n\'est pas possible de supprimer une Nouveau car elle contient des classes',
    'suprimer_checked' => 'supprimer les cas selectionne',
    'Voir tous' => 'Voir tous',
    'chercher par Niveau' => 'chercher par Niveau',


];
