<?php

return [
    'Teachers'=>'Enseignants',
    'List_Teachers' => 'Liste des enseignants',
    'Name_Teacher'=>'Nom de l\'enseignant',
    'Add_Teacher'=>'Ajouter un enseignant',
    'Edit_Teacher'=>'Modifier l\'enseignant',
    'Delete_Teacher'=>'Supprimer l\'enseignant',
    'Email'=>'Adresse e-mail',
    'Password'=>'mot de passe',
    'Name_ar'=>'Nom en arabe',
    'Name_fr'=>'Nom en français',
    'specialization'=>'Spécialisation',
    'Gender'=>'Genre',
    'Joining_Date'=>'Date d\'entrée en fonction',
    'Address'=>'Adresse',
    'Attention_teacher' => 'ete vous sur pour la suppression ?',
];