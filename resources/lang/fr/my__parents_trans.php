<?php 

return [
    'parents' => 'parents',
    'List_Parents'=>'liste des parents',
    'Add_Parent'=>'ajouter parent', 
    'add_parent' => 'ajouter parent',
    'Step1' => 'information de pere',
    'Step2' => 'information de mere',
    'Step3' => 'confirmer les informations',
    'Email' => 'Email',
    'Password' => 'mot de passe',
    'Name_Father' => 'nom de pere en arabe',
    'Name_Father_fr' => 'nom de pere en francais',
    'Job_Father' => 'nom de travaille en arabe',
    'Job_Father_fr' => 'nom de travaille en francais',
    'National_ID_Father' => 'numero didentite',
    'Passport_ID_Father' => 'numero du passport',
    'Phone_Father' => 'numero de telephone',
    'Nationality_Father_id' => 'nationalite',
    'Blood_Type_Father_id' => 'type de sang',
    'Religion_Father_id' => 'criter',
    'Address_Father' => 'addresse du pere',

    //معلومات الام
    'Name_Mother' => 'nom du mere en arabe',
    'Name_Mother_fr' => 'nom du mere en francais',
    'Job_Mother' => 'nom de travaille',
    'Job_Mother_fr' => 'nom de travaille en francais',
    'National_ID_Mother' => 'numero  didentite',
    'Passport_ID_Mother' => 'numero du passport',
    'Phone_Mother' => 'numero de telephone',
    'Address_Mother' => 'addresse du mere',

    'Next' => 'suivant',
    'Back' => 'annuler',
    'Finish' => 'terminer',
    'Choose' => 'selectionner',
    'Attachments' => 'les images',
    'Processes' => 'opperation',
    'parent_confirmation' => 'ete vous sur pour l\'enregistrement des informations'
];