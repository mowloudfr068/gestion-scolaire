<?php

return [

    'les Sections' => 'les Sections',
    'Liste_section' => 'Liste des sections',
    'Ajouter_section' => 'Ajouter section',
    'Modifier_section' => 'Modifier section',
    'Nom' => 'Nom',
    'Supprimer_section' => 'Supprimer section',
    'Supprimer_lignes' => 'Supprimer_lignes',
    'confirm_supp_sec' => 'ete vous sur pour la suppression ?',
    'Veuillez_section_arabe' => 'Veuillez_section_arabe',
    'Veuillez_section_français' => 'Veuillez_section_français',
    'section_arabe' => 'section_arabe',
    'section_français' => 'section_français',
    'nom_Niveau' => 'nom Niveau',
    'nom_classe' => 'nom classe',
    'Voir tous' => 'Voir tous',
    'activer' => 'activer',
    'desactiver' => 'desactiver',
    'status' => 'statut'



];
