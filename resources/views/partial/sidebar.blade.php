<div class="sidebar-wrapper sidebar-theme">
    <nav id="sidebar">
        <ul class="navbar-nav theme-brand flex-row  text-center">
            <li class="nav-item theme-text">
                <a href="index.html" class="nav-link"> ISLAH Eraide </a>
            </li>
            <li class="nav-item toggle-sidebar">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                    class="feather sidebarCollapse feather-chevrons-left">
                    <polyline points="11 17 6 12 11 7"></polyline>
                    <polyline points="18 17 13 12 18 7"></polyline>
                </svg>
            </li>
        </ul>
        <div class="shadow-bottom"></div>
        <ul class="list-unstyled menu-categories" id="accordionExample">
            <li class="menu active">
                <a href="#dashboard" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-home">
                            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                            <polyline points="9 22 9 12 15 12 15 22"></polyline>
                        </svg>
                        <span>{{__('common_trans.Tableau De Bord')}}</span>
                    </div>
                    <div>
                      
                    </div>
                </a>
              
            </li>
            <li class="menu">
                <a href="#components" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layers">
                            <polygon points="12 2 2 7 12 12.5 22 7 12 2"></polygon>
                            <polyline points="2 17 12 22.5 22 17"></polyline>
                            <polyline points="2 12 12 17.5 22 12"></polyline>
                          </svg>                          
                        <span>{{ __('grades_trans.Niveau scolaires') }}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="components" data-parent="#accordionExample">
                    <li>
                        <a href="{{ route('grades.index')  }}">{{ __('grades_trans.List niveau scolaires') }}</a>
                    </li>
                </ul>
            </li>
            <li class="menu">
                <a href="#elements" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" 
                        stroke-linejoin="round" class="feather feather-book-open">
                        <path d="M6 4v16a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4"></path>
                        <path d="M6 4L12 9l6-5"></path>
                        </svg>

                        <span>{{__('classrooms_trans.les classes')}}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="elements" data-parent="#accordionExample">
                    <li>
                        <a href="{{ route('classrooms.index')  }}"> {{__('classrooms_trans.Liste_classe')}} </a>
                    </li>
                </ul>
            </li>
            <li class="menu">
                <a href="#section" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layers">
                            <polygon points="12 2 2 7 12 12.5 22 7 12 2"></polygon>
                            <polyline points="2 17 12 22.5 22 17"></polyline>
                            <polyline points="2 12 12 17.5 22 12"></polyline>
                          </svg>                                                   
                        <span>{{__('sections_trans.les Sections')}}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="section" data-parent="#accordionExample">
                    <li>
                        <a href="{{  route('sections.index')  }}"> {{ __('sections_trans.Liste_section') }} </a>
                    </li>
                </ul>
            </li>

            <li class="menu">
                <a href="#etudiant" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" class="feather feather-user">
                    <path d="M2 22c0-5.5 4.5-10 10-10h0c5.5 0 10 4.5 10 10"></path>
                    <circle cx="12" cy="7" r="4"></circle>
                    </svg>

                        <span>{{ __('my__parents_trans.parents')  }}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="etudiant" data-parent="#accordionExample">
                    <li>
                        <a href="{{ url('add_parent') }}"> {{ __('my__parents_trans.List_Parents')  }} </a>
                    </li>
                </ul>
            </li>
            <li class="menu">
                <a href="#Teachers" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users">
                            <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                            <circle cx="9" cy="7" r="4"></circle>
                            <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                            <path d="M22.5 8.5l-2.2 2.2"></path>
                            <path d="M6.5 8.5l2.2 2.2"></path>
                          </svg>
                          
                        <span>{{ __('teachers_trans.Teachers')  }}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="Teachers" data-parent="#accordionExample">
                    <li>
                        <a href="{{ route('Teachers.index') }}"> {{ __('teachers_trans.List_Teachers')  }} </a>
                    </li>
                </ul>
            </li>
            <li class="menu">
                <a href="#Students" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users">
                            <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                            <circle cx="9" cy="7" r="4"></circle>
                            <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                            <path d="M22.5 8.5l-2.2 2.2"></path>
                            <path d="M6.5 8.5l2.2 2.2"></path>
                          </svg>
                          
                        <span>{{ __('students_trans.students')  }}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="Students" data-parent="#accordionExample">
                    <li>
                        <a href="{{ route('Students.create') }}"> {{ __('students_trans.add_student')  }} </a>
                    </li>
                    <li>
                        <a href="{{ route('Students.index') }}"> {{ __('students_trans.students')  }} </a>
                    </li>
                    <li>
                        <a href="{{ route('Promotion.index') }}"> {{ __('students_trans.Students_Promotions')  }} </a>
                    </li>
                    <li>
                        <a href="{{ route('Promotion.create') }}"> {{ __('students_trans.list_Promotions')  }} </a>
                    </li>

                    <li>
                        <a href="{{ route('Graduated.index') }}"> {{ __('students_trans.list_Graduate')  }} </a>
                    </li>
                    <li>
                        <a href="{{ route('Graduated.create') }}"> {{ __('students_trans.add_Graduate')  }} </a>
                    </li>
                </ul>
            </li>

        </ul>

    </nav>

</div>
