<title>@yield('title')</title>

<link rel="icon" type="image/x-icon" href="{{ asset('assets/img/favicon.ico') }}" />
<link href="{{ asset('assets/css' . LaravelLocalization::getCurrentLocaleDirection() . '/loader.css') }}" rel="stylesheet"
    type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/css/wizard.css') }}">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
<link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ asset('Labrary/Font_Awsome/css/all.min.css') }}">
<link href="{{ asset('assets/css' . LaravelLocalization::getCurrentLocaleDirection() . '/plugins.css') }}"
    rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/table/datatable/datatables.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/select2/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/table/datatable/dt-global_style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/table/datatable/custom_dt_multiple_tables.css') }}">
<link
    href="{{ asset('/assets/css' . LaravelLocalization::getCurrentLocaleDirection() . '/components/tabs-accordian/custom-accordions.css') }}"
    rel="stylesheet" type="text/css" />

<!-- END GLOBAL MANDATORY STYLES -->
{{-- @yield('css') --}}
<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
<link href="{{ asset('plugins/apex/apexcharts.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css' . LaravelLocalization::getCurrentLocaleDirection() . '/dashboard/dash_1.css') }}"
    rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
