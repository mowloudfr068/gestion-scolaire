@extends('layouts.master')
@section('css')
@endsection
@section('title')
    {{ __('grades_trans.Niveau scolaires') }}
@endsection
@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div
                @if (LaravelLocalization::getCurrentLocale() == 'ar') class="col-xl-3 col-md-3 col-sm-3 col-3"
            @else
            class="col-xl-9 col-md-9 col-sm-9 col-9" @endif>
                <div class="mt-2">
                    <a href="#">{{ __('common_trans.Home') }}</a>/<a
                        href="#">{{ __('classrooms_trans.Liste_classe') }}</a>
                </div>
            </div>
            <div
                @if (LaravelLocalization::getCurrentLocale() == 'ar') class="col-xl-9 col-md-9 col-sm-9 col-9"
            @else
            class="col-xl-3 col-md-3 col-sm-3 col-3" @endif>
                <button type="button" class="btn btn-success x-small" data-toggle="modal" data-target="#exampleModal">
                    {{ __('classrooms_trans.Ajouter_classe') }}
                </button>

            </div>

        </div>

    </div>
@endsection
@section('content')
    <div class="row m-2">

        <button type="button" class="button  btn btn-success x-small" id="btn_delete_all">
            {{ __('classrooms_trans.suprimer_checked') }}
        </button>
        <div class="m-2">
            <form action="{{ route('classrooms.filter_classes') }}" method="POST">
                {{ csrf_field() }}
                <select class="form-control" data-style="btn-info" name="grade_id" required onchange="this.form.submit()">

                    <option value="" selected disabled>{{ __('classrooms_trans.chercher par Niveau') }}</option>
                    <option value="all">{{ __('classrooms_trans.Voir tous') }}</option>
                    @foreach ($grades as $grade)
                        <option value="{{ $grade->id }}">{{ $grade->Name }}</option>
                    @endforeach
                </select>
            </form>
        </div>

    </div>


    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <table class="multi-table table table-striped table-bordered table-hover non-hover" id="dataTable" style="width:100%">
        <thead>
            <tr>
                <th><input name="select_all" id="example-select-all" type="checkbox" onclick="CheckAll('box1', this)" />
                </th>
                <th>#</th>
                <th>{{ __('classrooms_trans.Nom') }}</th>
                <th>{{ __('classrooms_trans.nom_Niveau') }}</th>
                <th class="text-center dt-no-sorting">{{ __('common_trans.Opperation') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($classes as $classe)
                <tr>
                    <td><input type="checkbox" value="{{ $classe->id }}" class="box1"></td>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $classe->Name }}</td>
                    <td>{{ $classe->grade->Name }}</td>
                    <td class="text-center">
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                            data-target="#edit{{ $classe->id }}"
                            title="{{ __('common_trans.Modifier') }}">{{ __('common_trans.Modifier') }}</button>

                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                            data-target="#delete{{ $classe->id }}"
                            title="{{ __('common_trans.Supprimer') }}">{{ __('common_trans.Supprimer') }}</button>
                    </td>
                </tr>
                <!-- edit_modal_classe -->
                <div class="modal fade" id="edit{{ $classe->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                                    {{ __('classrooms_trans.Modifier_classe') }}
                                </h5>

                            </div>
                            <div class="modal-body">
                                <!-- edit_form -->
                                <form action="{{ route('classrooms.update', 'test') }}" method="post">
                                    {{ method_field('patch') }}
                                    @csrf
                                    <div class="row">
                                        <div class="col">
                                            <label for="Name_ar" class="mr-sm-2">{{ __('classrooms_trans.classe_arabe') }}
                                                :</label>
                                            <input id="Name_ar" type="text" name="Name_ar" class="form-control"
                                                value="{{ $classe->getTranslation('Name', 'ar') }}">
                                            <input id="id" type="hidden" name="id" class="form-control"
                                                value="{{ $classe->id }}">
                                        </div>
                                        <div class="col">
                                            <label for="Name_fr"
                                                class="mr-sm-2">{{ __('classrooms_trans.classe_français') }}
                                                :</label>
                                            <input type="text" class="form-control"
                                                value="{{ $classe->getTranslation('Name', 'fr') }}" name="Name_fr">
                                        </div>
                                    </div><br>
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">{{ __('classrooms_trans.nom_Niveau') }}
                                            :</label>
                                        <select class="form-control form-control-lg" id="exampleFormControlSelect1"
                                            name="grade_id">
                                            @foreach ($grades as $grade)
                                                <option value="{{ $grade->id }}"
                                                    @if ($classe->grade->id == $grade->id) selected @endif>
                                                    {{ $grade->Name }}
                                                </option>
                                            @endforeach
                                        </select>

                                    </div>
                                    <br><br>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">{{ __('common_trans.Annuler') }}</button>
                                        <button type="submit"
                                            class="btn btn-success">{{ __('common_trans.enregistre') }}</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- delete_modal_Grade -->
                <div class="modal fade" id="delete{{ $classe->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                                    {{ __('classrooms_trans.Supprimer_classe') }}
                                </h5>

                            </div>
                            <div class="modal-body">
                                <form action="{{ route('classrooms.destroy', 'test') }}" method="post">
                                    {{ method_field('Delete') }}
                                    @csrf
                                    {{ __('classrooms_trans.confirm_supp_cls') }}
                                    <input id="id" type="hidden" name="id" class="form-control"
                                        value="{{ $classe->id }}">
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">{{ __('common_trans.Annuler') }}</button>
                                        <button type="submit"
                                            class="btn btn-danger">{{ __('common_trans.enregistre') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </tbody>

    </table>

    <!-- add_modal_class -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                        {{ __('classrooms_trans.Ajouter_classe') }}
                    </h5>

                </div>
                <div class="modal-body">

                    <form class=" row mb-30" action="{{ route('classrooms.store') }}" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="repeater">
                                <div data-repeater-list="list_classes">
                                    <div data-repeater-item>
                                        <div class="row">

                                            <div class="col">
                                                <label for="Name"
                                                    class="mr-sm-2">{{ __('classrooms_trans.classe_arabe') }}
                                                    :</label>
                                                <input class="form-control" type="text" name="Name_ar" />
                                            </div>


                                            <div class="col">
                                                <label for="Name"
                                                    class="mr-sm-2">{{ __('classrooms_trans.classe_français') }}
                                                    :</label>
                                                <input class="form-control" type="text" name="Name_fr" />
                                            </div>


                                            <div class="col">
                                                <label for="Name_en"
                                                    class="mr-sm-2">{{ __('classrooms_trans.nom_Niveau') }}
                                                    :</label>

                                                <div class="box">
                                                    <select class="form-control" name="grade_id">
                                                        @foreach ($grades as $grade)
                                                            <option value="{{ $grade->id }}">{{ $grade->Name }}
                                                            </option>
                                                        @endforeach
                                                    </select>

                                                </div>

                                            </div>

                                            <div class="col">
                                                <label for="Name_en" class="mr-sm-2">{{ __('common_trans.Opperation') }}
                                                    :</label>
                                                <input class="btn btn-danger  btn-block" data-repeater-delete
                                                    type="button"
                                                    value="{{ __('classrooms_trans.Supprimer_ligne') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-12 mb-2">
                                        <input class="button btn btn-info btn-sm" data-repeater-create type="button"
                                            value="{{ __('classrooms_trans.Ajouter_ligne') }}" />
                                    </div>

                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-dismiss="modal">{{ __('common_trans.Annuler') }}</button>
                                    <button type="submit"
                                        class="btn btn-success">{{ __('common_trans.enregistre') }}</button>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>


            </div>

        </div>

    </div>
    <!-- modal confirmation suprimer un ou plusieur classrooms -->
    <div class="modal fade" id="delete_all" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                        {{ __('classrooms_trans.Supprimer_classe') }}
                    </h5>
                </div>

                <form action="{{ route('classrooms.delete_all') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        {{ __('classrooms_trans.confirm_supp_cls') }}
                        <input class="text" type="hidden" id="delete_all_id" name="delete_all_id" value=''>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                            data-dismiss="modal">{{ __('common_trans.Annuler') }}</button>
                        <button type="submit" class="btn btn-danger">{{ __('common_trans.Supprimer') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        //selectionnne tous
        function CheckAll(className, elem) {
            var elements = document.getElementsByClassName(className);
            var l = elements.length;

            if (elem.checked) {
                for (var i = 0; i < l; i++) {
                    elements[i].checked = true;
                }
            } else {
                for (var i = 0; i < l; i++) {
                    elements[i].checked = false;
                }
            }
        }


        //data table js
        $(document).ready(function() {
            $('table.multi-table').DataTable({
                "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                    "<'table-responsive'tr>" +
                    "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                        "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                    },
                    "sInfo": "Showing page _PAGE_ of _PAGES_",
                    "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                    "sSearchPlaceholder": "Search...",
                    "sLengthMenu": "Results :  _MENU_",
                },
                "stripeClasses": [],
                "lengthMenu": [7, 10, 20, 50],
                "pageLength": 7,
                drawCallback: function() {
                    $('.t-dot').tooltip({
                        template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                    })
                    $('.dataTables_wrapper table').removeClass('table-striped');
                }
            });
        });

        // supprimer tous recuperer les id
        $(function() {
            $("#btn_delete_all").click(function() {
                var selected = new Array();
                $("#dataTable input[type=checkbox]:checked").each(function() {
                    selected.push(this.value);
                });

                if (selected.length > 0) {
                    $('#delete_all').modal('show')
                    $('input[id="delete_all_id"]').val(selected);
                }
            });
        });
    </script>
@endsection
