@extends('layouts.master')
@section('css')
@endsection
@section('title')
    {{ __('sections_trans.Niveau scolaires') }}
@endsection
@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div
                @if (LaravelLocalization::getCurrentLocale() == 'ar') class="col-xl-3 col-md-3 col-sm-3 col-3"
            @else
            class="col-xl-9 col-md-9 col-sm-9 col-9" @endif>
                <div class="mt-2">
                    <a href="#">{{ __('common_trans.Home') }}</a>/
                    {{ __('sections_trans.les Sections') }}

                </div>
            </div>
            <div
                @if (LaravelLocalization::getCurrentLocale() == 'ar') class="col-xl-9 col-md-9 col-sm-9 col-9"
            @else
            class="col-xl-3 col-md-3 col-sm-3 col-3" @endif>
                <button type="button" class="btn btn-success x-small" data-toggle="modal" data-target="#exampleModal">
                    {{ __('sections_trans.Ajouter_section') }}
                </button>


            </div>


        </div>

    </div>
@endsection
@section('content')
    <div id="toggleAccordion">
        <div class="card">
            @foreach ($grades as $grade)
                <div class="card-header" id="headingOne{{ $grade->id }}">
                    <section class="mb-0 mt-0">
                        <div role="menu" class="collapsed text-center" data-toggle="collapse"
                            data-target="#defaultAccordion{{ $grade->id }}" aria-expanded="true"
                            aria-controls="defaultAccordionOne">
                            {{ $grade->Name }} <div class="icons"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                    height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                    stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                                    <polyline points="6 9 12 15 18 9"></polyline>
                                </svg></div>
                        </div>
                    </section>
                </div>

                <div id="defaultAccordion{{ $grade->id }}" class="collapse"
                    aria-labelledby="headingOne{{ $grade->id }}" data-parent="#toggleAccordion">
                    <div class="card-body">
                        <table class="multi-table table table-striped table-bordered table-hover non-hover"
                            style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{ __('sections_trans.Nom') }}</th>
                                    <th>{{ __('sections_trans.nom_Niveau') }}</th>
                                    <th>{{ __('sections_trans.status') }}</th>
                                    <th class="text-center dt-no-sorting">{{ __('common_trans.Opperation') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($grade->sections as $section_list)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $section_list->Name }}</td>
                                        <td>{{ $section_list->classroom->Name }}</td>
                                        <td>
                                            @if ($section_list->Status === 1)
                                                <label
                                                    class="badge badge-success">{{ __('sections_trans.activer') }}</label>
                                            @else
                                                <label
                                                    class="badge badge-danger">{{ __('sections_trans.desactiver') }}</label>
                                            @endif

                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-outline-info btn-sm" data-toggle="modal"
                                                data-target="#edit{{ $section_list->id }}">{{ __('common_trans.Modifier') }}</a>
                                            <a href="#" class="btn btn-outline-danger btn-sm" data-toggle="modal"
                                                data-target="#delete{{ $section_list->id }}">{{ __('common_trans.Supprimer') }}</a>
                                        </td>
                                    </tr>
                                    {{-- modal_modifier_section --}}
                                    <div class="modal fade" id="edit{{ $section_list->id }}" tabindex="-1" role="dialog"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" style="font-family: 'Cairo', sans-serif;"
                                                        id="exampleModalLabel">
                                                        {{ trans('sections_trans.Modifier_section') }}
                                                    </h5>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{ route('sections.update', 'test') }}" method="POST">
                                                        {{ method_field('patch') }}
                                                        {{ csrf_field() }}
                                                        <div class="row">
                                                            <div class="col">
                                                                <input type="text" name="Name_ar" class="form-control"
                                                                    value="{{ $section_list->getTranslation('Name', 'ar') }}">
                                                            </div>

                                                            <div class="col">
                                                                <input type="text" name="Name_fr" class="form-control"
                                                                    value="{{ $section_list->getTranslation('Name', 'fr') }}">
                                                                <input id="id" type="hidden" name="id"
                                                                    class="form-control" value="{{ $section_list->id }}">
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="col">
                                                            <label for="inputName"
                                                                class="control-label">{{ trans('sections_trans.nom_Niveau') }}</label>
                                                            <select name="grade_id" class="form-control"
                                                                onclick="console.log($(this).val())">
                                                                <!--placeholder-->
                                                                <option value="{{ $grade->id }}">
                                                                    {{ $grade->Name }}
                                                                </option>
                                                                @foreach ($list_grades as $list_grade)
                                                                    <option value="{{ $list_grade->id }}">
                                                                        {{ $list_grade->Name }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <br>

                                                        <div class="col">
                                                            <label for="inputName"
                                                                class="control-label">{{ trans('sections_trans.nom_classe') }}</label>
                                                            <select name="class_id" class="form-control">
                                                                <option value="{{ $section_list->classroom->id }}">
                                                                    {{ $section_list->classroom->Name }}
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <br>
                                                <div class="col">
                                                    <label for="inputName" class="control-label">{{ trans('teachers_trans.Name_Teacher') }}</label>
                                                    <select multiple name="teacher_id[]" class="form-control" id="exampleFormControlSelect2">
                                                        @foreach($section_list->teachers as $teacher)
                                                            <option selected value="{{$teacher['id']}}">{{$teacher['Name']}}</option>
                                                        @endforeach

                                                        @foreach($teachers as $teacher)
                                                            <option value="{{$teacher->id}}">{{$teacher->Name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                        <div class="col">
                                                            <div class="form-check">
                                                                @if ($section_list->Status === 1)
                                                                    <input type="checkbox" checked class="form-check-input"
                                                                        name="Status" id="exampleCheck1">
                                                                @else
                                                                    <input type="checkbox" class="form-check-input"
                                                                        name="Status" id="exampleCheck1">
                                                                @endif
                                                                <label class="form-check-label"
                                                                    for="exampleCheck1">{{ trans('sections_trans.status') }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">{{ trans('common_trans.Annuler') }}</button>
                                                    <button type="submit" class="btn btn-danger">
                                                        {{ trans('common_trans.enregistre') }}
                                                    </button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- delete_modal_Grade -->
                                    <div class="modal fade" id="delete{{ $section_list->id }}" tabindex="-1"
                                        role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title"
                                                        id="exampleModalLabel">
                                                        {{ trans('sections_trans.Supprimer_section') }}
                                                    </h5>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{ route('sections.destroy', 'test') }}"
                                                        method="post">
                                                        {{ method_field('Delete') }}
                                                        @csrf
                                                        {{ trans('sections_trans.confirm_supp_sec') }}
                                                        <input id="id" type="hidden" name="id"
                                                            class="form-control" value="{{ $section_list->id }}">
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{ trans('common_trans.Annuler') }}</button>
                                                            <button type="submit"
                                                                class="btn btn-danger">{{ trans('common_trans.Supprimer') }}</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
        </div>
        @endforeach
    </div>
    {{-- model_add_section --}}
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="font-family: 'Cairo', sans-serif;" id="exampleModalLabel">
                        {{ __('sections_trans.Ajouter_section') }}</h5>
                </div>
                <div class="modal-body">

                    <form action="{{ route('sections.store') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col">
                                <input type="text" name="Name_ar" class="form-control"
                                    placeholder="{{ __('sections_trans.section_arabe') }}">
                            </div>

                            <div class="col">
                                <input type="text" name="Name_fr" class="form-control"
                                    placeholder="{{ __('sections_trans.section_français') }}">
                            </div>

                        </div>
                        <br>


                        <div class="col">
                            <label for="inputName" class="control-label">{{ __('sections_trans.nom_Niveau') }}</label>
                            <select name="grade_id" class="form-control" onchange="console.log($(this).val())">
                                <!--placeholder-->
                                <option value="" selected disabled>{{ __('sections_trans.nom_Niveau') }}
                                </option>
                                @foreach ($list_grades as $list_grade)
                                    <option value="{{ $list_grade->id }}"> {{ $list_grade->Name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <br>

                        <div class="col">
                            <label for="inputName" class="control-label">{{ __('sections_trans.nom_classe') }}</label>
                            <select name="class_id" class="form-control">

                            </select>
                        </div><br>
                        <div class="col">
                            <label for="inputName" class="control-label">{{ trans('teachers_trans.Name_Teacher') }}</label>
                            <select multiple name="teacher_id[]" class="form-control" id="exampleFormControlSelect2">
                                @foreach($teachers as $teacher)
                                    <option value="{{$teacher->id}}">{{$teacher->Name}}</option>
                                @endforeach
                            </select>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">{{ __('common_trans.Annuler') }}</button>
                    <button type="submit" class="btn btn-danger">{{ __('common_trans.enregistre') }}</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('table.multi-table').DataTable({
            "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                "<'table-responsive'tr>" +
                "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                    "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
                "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7,
            drawCallback: function() {
                $('.t-dot').tooltip({
                    template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                })
                $('.dataTables_wrapper table').removeClass('table-striped');
            }
        });


        $(document).ready(function() {
            $('select[name="grade_id"]').on('change', function() {
                var grade_id = $(this).val();
                if (grade_id) {
                    $.ajax({
                        url: "{{ URL::to('classes') }}/" + grade_id,
                        type: "GET",
                        dataType: "json",
                        success: function(data) {
                            $('select[name="class_id"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="class_id"]').append('<option value="' +
                                    key + '">' + value + '</option>');
                            });
                        },
                    });
                } else {
                    console.log('AJAX erreur');
                }
            });
        });
    </script>
@endsection
