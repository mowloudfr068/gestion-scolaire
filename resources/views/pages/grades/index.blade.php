@extends('layouts.master')
@section('css')
@endsection
@section('title')
    {{ __('grades_trans.Niveau scolaires') }}
@endsection
@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div
                @if (LaravelLocalization::getCurrentLocale() == 'ar') class="col-xl-3 col-md-3 col-sm-3 col-3"
            @else
            class="col-xl-9 col-md-9 col-sm-9 col-9" @endif>
                <div class="mt-2">
                    <a href="#">{{ __('common_trans.Home') }}</a>/<a
                        href="#">{{ __('grades_trans.Niveau scolaires') }}</a>
                        
                </div>
            </div>
            <div
                @if (LaravelLocalization::getCurrentLocale() == 'ar') class="col-xl-9 col-md-9 col-sm-9 col-9"
            @else
            class="col-xl-3 col-md-3 col-sm-3 col-3" @endif>
                <button type="button" class="btn btn-success x-small" data-toggle="modal" data-target="#exampleModal">
                    {{ __('grades_trans.Ajouter Niveau') }}
                </button>
                

            </div>
            

        </div>

    </div>
@endsection
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <table class="multi-table table table-striped table-bordered table-hover non-hover" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('grades_trans.Nom') }}</th>
                <th>{{ __('common_trans.Observation') }}</th>
                <th class="text-center dt-no-sorting">{{ __('common_trans.Opperation') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($grades as $grade)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $grade->Name }}</td>
                    <td>{{ $grade->Notes }}</td>
                    <td class="text-center">
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                            data-target="#edit{{ $grade->id }}"
                            title="{{ __('common_trans.Modifier') }}">{{ __('common_trans.Modifier') }}</button>

                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                            data-target="#delete{{ $grade->id }}"
                            title="{{ __('common_trans.Supprimer') }}">{{ __('common_trans.Supprimer') }}</button>
                    </td>
                </tr>
                <!-- edit_modal_Grade -->
                <div class="modal fade" id="edit{{ $grade->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                                    {{ __('grades_trans.Modifier Niveau') }}
                                </h5>
                            </div>
                            <div class="modal-body">
                                <!-- add_form -->
                                <form action="{{ route('grades.update', 'test') }}" method="post">
                                    {{ method_field('patch') }}
                                    @csrf
                                    <div class="row">
                                        <div class="col">
                                            <label for="Name_ar" class="mr-sm-2">{{ __('grades_trans.Niveau_nom_ar') }}
                                                :</label>
                                            <input id="Name" type="text" name="Name_ar" class="form-control"
                                                value="{{ $grade->getTranslation('Name', 'ar') }}" required>
                                            <input id="id" type="hidden" name="id" class="form-control"
                                                value="{{ $grade->id }}">
                                        </div>
                                        <div class="col">
                                            <label for="Name_fr" class="mr-sm-2">{{ __('grades_trans.Niveau_nom_fr') }}
                                                :</label>
                                            <input type="text" class="form-control"
                                                value="{{ $grade->getTranslation('Name', 'fr') }}" name="Name_fr" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">{{ __('common_trans.Observation') }}
                                            :</label>
                                        <textarea class="form-control" name="Notes" id="exampleFormControlTextarea1" rows="3">{{ $grade->Notes }}</textarea>
                                    </div>
                                    <br><br>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">{{ __('common_trans.Annuler') }}</button>
                                        <button type="submit"
                                            class="btn btn-success">{{ __('common_trans.Modifier') }}</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- delete_modal_Grade -->
                <div class="modal fade" id="delete{{ $grade->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                                    {{ __('grades_trans.supprimer Niveau') }}
                                </h5>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('grades.destroy', 'test') }}" method="post">
                                    {{ method_field('Delete') }}
                                    @csrf
                                    {{ __('grades_trans.Attention Niveau') }}
                                    <input id="id" type="hidden" name="id" class="form-control"
                                        value="{{ $grade->id }}">
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">{{ __('common_trans.Annuler') }}</button>
                                        <button type="submit"
                                            class="btn btn-danger">{{ __('common_trans.Supprimer') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </tbody>

    </table>

    <!-- add_modal_Grade -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                        {{ __('grades_trans.Ajouter Niveau') }}
                    </h5>

                </div>
                <div class="modal-body">
                    <!-- add_form -->
                    <form action="{{ route('grades.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <label for="Name_ar" class="mr-sm-2">{{ __('grades_trans.Niveau_nom_ar') }}
                                    :</label>
                                <input id="Name_ar" type="text" name="Name_ar" class="form-control">
                            </div>
                            <div class="col">
                                <label for="Name_fr" class="mr-sm-2">{{ __('grades_trans.Niveau_nom_fr') }}
                                    :</label>
                                <input type="text" class="form-control" name="Name_fr">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">{{ __('common_trans.Observation') }}
                                :</label>
                            <textarea class="form-control" name="Notes" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        <br><br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">{{ __('common_trans.Annuler') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('common_trans.enregistre') }}</button>
                </div>
                </form>

            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $('table.multi-table').DataTable({
                "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                    "<'table-responsive'tr>" +
                    "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                        "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                    },
                    "sInfo": "Showing page _PAGE_ of _PAGES_",
                    "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                    "sSearchPlaceholder": "Search...",
                    "sLengthMenu": "Results :  _MENU_",
                },
                "stripeClasses": [],
                "lengthMenu": [7, 10, 20, 50],
                "pageLength": 7,
                drawCallback: function() {
                    $('.t-dot').tooltip({
                        template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                    })
                    $('.dataTables_wrapper table').removeClass('table-striped');
                }
            });
        });
    </script>
@endsection
