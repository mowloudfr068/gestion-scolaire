<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
   
     @yield('css')   
     
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    @include('partial.head')
    
</head>

<body>
    <!--  BEGIN NAVBAR  -->
    @include('partial.header')
    <!--  END NAVBAR  -->

    <!--  BEGIN MAIN CONTAINER  -->

    <div class="main-container" id="container">
        <div class="overlay"></div>
        <div class="search-overlay"></div>

        <!--  BEGIN SIDEBAR  -->
        @include('partial.sidebar')
        <!--  END SIDEBAR  -->

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div id="tableDropdown" class="col-lg-12 col-12 layout-spacing">
                        <div class="statbox widget box box-shadow">
                            @yield('content-header')
                            <div class="widget-content widget-content-area">
                                @yield('content')
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            @include('partial.footer')
        </div>
        <!--  END CONTENT AREA  -->

    </div>
    <!-- END MAIN CONTAINER -->

    <!-- BEGIN  SCRIPTS -->
    @include('partial.footer-scripts')
    @yield('js')
    <!-- END    SCRIPTS -->
</body>

</html>
