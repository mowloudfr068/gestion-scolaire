@extends('layouts.master')
@section('title')
    {{ __('my__parents_trans.Add_Parent') }}
@endsection

@section('css')
    @livewireStyles
@endsection
@section('content-header')
{{ __('my__parents_trans.Add_Parent') }}    
@endsection
@section('content')
    @livewire('add-parent')
@endsection

@section('js')
    @livewireScripts
    <script>
        $(document).ready(function() {
        $('#datatable').DataTable();
    } );
    </script>
    
@endsection