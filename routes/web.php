<?php

use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;



Auth::routes();

Route::group(['middleware' => 'guest'], function () {
    Route::get('/', function () {
        return view('auth.login');
    });
});



Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'auth']
    ],
    function () {
        Route::get('empty', function () {
            return view('empty');
        });


        Route::get('dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

        ##########   grades ###############
        Route::group(['namespace' => 'Grades'], function () {

            Route::resource('grades', 'GradeController');
        });


        ##########  classrooms  ############
        Route::group(['namespace' => 'Classrooms'], function () {

            Route::resource('classrooms', 'ClassroomController');
            Route::post('delete_all', 'ClassroomController@delete_all')->name('classrooms.delete_all');
            Route::post('filter_classes', 'ClassroomController@filter_classes')->name('classrooms.filter_classes');
        });
        ##########  sections  ##############
        Route::group(['namespace' => 'Sections'], function () {

            Route::resource('sections', 'SectionController');
            Route::get('/classes/{id}', 'SectionController@getClassrooms');
        });

        ##########  parents  ##############
        Route::view('add_parent', 'livewire.show_form');
        //==============================Teachers============================
        Route::group(['namespace' => 'Teachers'], function () {
            Route::resource('Teachers', 'TeacherController');
        });
        //==============================Students============================
        Route::group(['namespace' => 'Students'], function () {
            Route::resource('Students', 'StudentController');
            Route::get('/Get_classrooms/{id}','StudentController@Get_classrooms');
            Route::get('/Get_Sections/{id}','StudentController@Get_Sections');
            Route::post('Upload_attachment', 'StudentController@Upload_attachment')->name('Upload_attachment');
            Route::get('Download_attachment/{studentsname}/{filename}', 'StudentController@Download_attachment')->name('Download_attachment');
            Route::post('Delete_attachment', 'StudentController@Delete_attachment')->name('Delete_attachment');
            Route::resource('Promotion', 'PromotionController');
            Route::resource('Graduated', 'GraduatedController');
        });
        

    }
);
